//========================================================================================
//	mongodb.js
//
//	This module is to perform CRUD operations with mongo db.
//
//	Company: Bizar Mobile
//	Date: 15/08/2013
//	Version: 0.0.3
//
//========================================================================================

var Db = require('mongodb').Db;
var Server = require('mongodb').Server;
var ReplSet = require('mongodb').ReplSet;
var ObjectID = require('mongodb').ObjectID;
var Binary = require('mongodb').Binary;
var GridStore = require('mongodb').GridStore;
var Grid = require('mongodb').Grid;
var Code = require('mongodb').Code;
var BSON = require('mongodb').pure().BSON;
var format = require('util').format;
var MongoClient = require('mongodb').MongoClient;

// Load database config based on server enviornment mode.

var env = process.env.NODE_ENV || 'development'
  , config = require('../config')[env]

var db = {};
//--------------------------------------------------------------------------------------------------------------
//	connect
//
//	This function connects to the database
//	Call:	db = dbm.connect();
//--------------------------------------------------------------------------------------------------------------

//exports.connect = function() {
exports.connect = function() {
	try {
		var url = '';
		switch (env) {
			case 'staging':
			case 'production' :
			case 'development' :
				url = format("mongodb://%s:%s/" + config.db.dbName, config.db.dbHost, config.db.dbPort)
				break;
//			case 'staging':
//			case 'production' :
//				url = format("mongodb://%s:%s,%s:%s,%s:%s/" +  config.db.dbName + "?replicaSet=" + config.db.replicaSetName, config.db.dbHost,
//				             config.db.dbPort1, config.db.dbHost2, config.db.dbPort2, config.db.dbHost3, config.db.dbPort3);
//				break;
			default:
				break;
		}
		MongoClient.connect(url, function (err, _db){
			if (err) throw err;
			console.log('Connected to mongodb');
			db = _db;
		})
	} catch(e) {
		console.log('error in dbmongo.connection ' + e);
	}
};
//--------------------------------------------------------------------------------------------------------------
//	count
//
//	This function queries the database and returns the result set
//--------------------------------------------------------------------------------------------------------------
exports.count = function (params, callback) {
	try {
//		dbh.open(function(err, db) {
			db.collection(params.collection, function(err, collection) {
				collection.count(function(err, count) {
					console.log(format("count = %s", count));
					callback(null, count);
				});
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.find: ' + e);
	}
 };
//--------------------------------------------------------------------------------------------------------------
//	find
//
//	This function queries the database and returns the result set
//--------------------------------------------------------------------------------------------------------------
exports.find = function (params, callback) {
	try {
//		dbh.open(function(err, db) {
			db.collection(params.collection, function(err, collection) {
				var cursor = collection.find(params.find, params.fields ? params.fields : []);
				if (params.sort) {cursor.sort(params.sort);}
				if (params.skip) {cursor.skip(params.skip);}
				if (params.limit) {cursor.limit(params.limit);}
				cursor.toArray(function(err, res) {
					if (err) throw err;
					callback(null, res);
//					db.close();
				});
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.find: ' + e);
	}
 };
//--------------------------------------------------------------------------------------------------------------
//	findAndModify
//
//	This function updates the desired document and returns back the updated object
//--------------------------------------------------------------------------------------------------------------

exports.findAndModify = function (params, callback) {
	try {
//		dbh.open(function(err, db) {
			db.collection(params.collection, function(err, collection) {
				collection.findAndModify(
					params.findAndModify,
					params.sort,
					params.update,
					params.options,
					function(err, object) {
//						db.close();
						callback(null, object);
					}
				);
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.findAndModify: ' + e);
	}
};

//--------------------------------------------------------------------------------------------------------------
//	findOne
//
//	This function find one document as per the passed findOne json object.
//--------------------------------------------------------------------------------------------------------------

exports.findOne = function (params, callback) {
	try {
//		dbh.open(function(err, db) {
			db.collection(params.collection, function(err, collection) {
				collection.findOne(
					params.findOne,
					function(err, object) {
//						db.close();
						callback(null, object);
					}
				);
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.findOne: ' + e);
	}
};

//--------------------------------------------------------------------------------------------------------------
//	findOneWeb
//
//	This function find one document as per the passed findOne json object.
//--------------------------------------------------------------------------------------------------------------

exports.findOneWeb = function (req, res, params, callback) {
	try {
//		dbh.open(function(err, db) {
			db.collection(params.collection, function(err, collection) {
				collection.findOne(
					params.findOne,
					function(err, object) {
//						db.close();
						callback(req, res, params, object);
					}
				);
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.findOneWeb: ' + e);
	}
}
//--------------------------------------------------------------------------------------------------------------
//	insert
//
//	This function inserts a document into a collection
//--------------------------------------------------------------------------------------------------------------
 exports.insert = function (params, callback) {
	try {
//		dbh.open(function(err, db) {
			db.collection(params.collection, function(err, collection) {
		//		if (callback) {
		//			collection.insert(params.insert, {safe: true}, function(err, objects) {
		//				callback(params, 'ok');
		//			});
		//		} else {
					collection.insert(params.insert, {w:1}, function(err, objects) {
						if (err) console.warn(err.message);
						if (err && err.message.indexOf('E11000 ') !== -1) {
						// this _id was already inserted in the database
						}
						if (callback) {callback(err, objects);}
      				});
//					db.close();
		//		}
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.insert: ' + e);
	}
};
//--------------------------------------------------------------------------------------------------------------
//	update
//
//	This function updates a document based on the update query
//--------------------------------------------------------------------------------------------------------------
exports.update = function(params, callback) {
	try {
//		dbh.open(function(err, db) {
//			console.log('dbmongo.update:' + JSON.stringify(params));
			db.collection(params.collection, function(err, collection) {
				collection.update(
					params.update,
					params.objNew,
					params.options,
					function(err, object) {
						if (callback) {callback(err, object);}
					}
				);
//				db.close();
			});
//		});
	} catch(e) {
		console.log('error in mongodb.update ' + e);
	}
};
//--------------------------------------------------------------------------------------------------------------
//	getNextSequence
//
//	This function auto increments a sequence and returns the updated object.
//--------------------------------------------------------------------------------------------------------------
exports.getNextSequence = function (sequenceName, callback) {
	try {
//		db.open(function(err, db) {
		console.log('getNextSequence');
			db.collection('sequenceCollection', function(err, collection) {
			console.log('getNextSequence2:' + sequenceName);
				collection.findAndModify(
					{_id: sequenceName},
					{},
					{$inc: {seq: 1}},
					{upsert: true},
					function(err, object) {
//						db.close();
						console.log(err);
						console.log(JSON.stringify(object));
						if (err) throw err;
						callback(null, object);
					}
				);
			});
//		});
	} catch (e) {
		console.log('Error caught in mongodb.getNextSequence: ' + e);
	}
};

//==============================================================================================================
//	mongodb.js
//==============================================================================================================
